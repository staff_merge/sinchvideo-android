package com.jobs.sinchvideocallingsample

import android.Manifest
import android.annotation.TargetApi
import android.app.*
import android.app.ActivityManager.RunningAppProcessInfo
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import com.jobs.sinchvideocallingsample.SinchService
import com.jobs.sinchvideocallingsample.fcm.FcmListenerService
import com.sinch.android.rtc.*
import com.sinch.android.rtc.calling.Call
import com.sinch.android.rtc.calling.CallClient
import com.sinch.android.rtc.calling.CallClientListener
import com.sinch.android.rtc.video.VideoController

class SinchService : Service() {
    private var messenger: Messenger? = null
    private var mSettings: PersistedSettings? = null
    private val mSinchServiceInterface = SinchServiceInterface()
    private var mSinchClient: SinchClient? = null
    private var mListener: StartFailedListener? = null

    override fun onCreate() {
        super.onCreate()
        mSettings = PersistedSettings(
                applicationContext
        )
        attemptAutoStart()
    }

    private fun attemptAutoStart() {
        if (messenger != null) {
            start()
        }
    }

    private fun createClient(username: String?) {
        mSinchClient =
                Sinch.getSinchClientBuilder().context(applicationContext).userId(username)
                        .applicationKey(APP_KEY)
                        .applicationSecret(APP_SECRET)
                        .environmentHost(ENVIRONMENT).build()
        mSinchClient.also {
            it?.setSupportCalling(true)
            it?.setSupportManagedPush(true)
            it?.startListeningOnActiveConnection()
            it?.addSinchClientListener(MySinchClientListener())
            it?.callClient?.addCallClientListener(SinchCallClientListener())
            it?.setPushNotificationDisplayName("User $username")
        }
    }

    override fun onDestroy() {
        if (mSinchClient != null && mSinchClient!!.isStarted) {
            mSinchClient!!.terminateGracefully()
        }
        super.onDestroy()
    }

    private fun hasUsername(): Boolean {
        if (mSettings!!.username!!.isEmpty()) {
            Log.e(
                    TAG,
                    "Can't start a SinchClient as no username is available!"
            )
            return false
        }
        return true
    }

    private fun createClientIfNecessary() {
        if (mSinchClient != null) return
        check(hasUsername()) { "Can't create a SinchClient as no username is available!" }
        createClient(mSettings!!.username)
    }

    private fun start() {
        var permissionsGranted = true
        createClientIfNecessary()
        try {
            //mandatory checks
            mSinchClient!!.checkManifest()
            //auxiliary check
            if (applicationContext.checkCallingOrSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                throw MissingPermissionException(Manifest.permission.CAMERA)
            }
        } catch (e: MissingPermissionException) {
            permissionsGranted = false
            if (messenger != null) {
                val message = Message.obtain()
                val bundle = Bundle()
                bundle.putString(
                        REQUIRED_PERMISSION,
                        e.requiredPermission
                )
                message.data = bundle
                message.what = MESSAGE_PERMISSIONS_NEEDED
                try {
                    messenger!!.send(message)
                } catch (e1: RemoteException) {
                    e1.printStackTrace()
                }
            }
        }
        if (permissionsGranted) {
            Log.d(TAG, "Starting SinchClient")
            try {
                mSinchClient!!.start()
            } catch (e: IllegalStateException) {
                Log.w(
                        TAG,
                        "Can't start SinchClient - " + e.message
                )
            }
        }
    }

    private fun stop() {
        if (mSinchClient != null) {
            mSinchClient!!.terminateGracefully()
            mSinchClient = null
        }
    }

    private val isStarted: Boolean
        private get() = mSinchClient != null && mSinchClient!!.isStarted

    override fun onBind(intent: Intent): IBinder? {
        messenger =
                intent.getParcelableExtra(MESSENGER)
        return mSinchServiceInterface
    }

    inner class SinchServiceInterface : Binder() {
        fun callUserVideo(userId: String?): Call {
            return mSinchClient!!.callClient.callUserVideo(userId)
        }

        var username: String?
            get() = mSettings!!.username
            set(username) {
                mSettings!!.username = username
            }

        fun retryStartAfterPermissionGranted() {
            attemptAutoStart()
        }

        val isStarted: Boolean
            get() = this@SinchService.isStarted

        fun startClient() {
            start()
        }

        fun stopClient() {
            stop()
        }

        fun getManagedPush(): ManagedPush? {
            // create client, but you don't need to start it
            createClientIfNecessary()
            // retrieve ManagedPush
            return Beta.createManagedPush(mSinchClient)
        }

        fun setStartListener(listener: StartFailedListener?) {
            mListener = listener
        }

        fun getCall(callId: String?): Call {
            return mSinchClient!!.callClient.getCall(callId)
        }

        val videoController: VideoController?
            get() = if (!isStarted) {
                null
            } else mSinchClient!!.videoController

        val audioController: AudioController?
            get() = if (!isStarted) {
                null
            } else mSinchClient!!.audioController

        fun relayRemotePushNotificationPayload(payload: MutableMap<String, String>?): NotificationResult? {
            if (!hasUsername()) {
                Log.e(
                        TAG,
                        "Unable to relay the push notification!"
                )
                return null
            }
            createClientIfNecessary()
            return mSinchClient?.relayRemotePushNotificationPayload(payload)
        }
    }

    interface StartFailedListener {
        fun onStartFailed(error: SinchError?)
        fun onStarted()
    }

    private inner class MySinchClientListener : SinchClientListener {
        override fun onClientFailed(client: SinchClient, error: SinchError) {
            if (mListener != null) {
                mListener!!.onStartFailed(error)
            }
            mSinchClient!!.terminate()
            mSinchClient = null
        }

        override fun onClientStarted(client: SinchClient) {
            Log.d(TAG, "SinchClient started")
            if (mListener != null) {
                mListener!!.onStarted()
            }
        }

        override fun onClientStopped(client: SinchClient) {
            Log.d(TAG, "SinchClient stopped")
        }

        override fun onLogMessage(
                level: Int,
                area: String,
                message: String
        ) {
            when (level) {
                Log.DEBUG -> Log.d(area, message)
                Log.ERROR -> Log.e(area, message)
                Log.INFO -> Log.i(area, message)
                Log.VERBOSE -> Log.v(area, message)
                Log.WARN -> Log.w(area, message)
            }
        }

        override fun onRegistrationCredentialsRequired(
                client: SinchClient,
                clientRegistration: ClientRegistration
        ) {
        }
    }

    private inner class SinchCallClientListener :
            CallClientListener {
        override fun onIncomingCall(
                callClient: CallClient,
                call: Call
        ) {
            Log.d(TAG, "onIncomingCall: " + call.callId)
            val intent = Intent(
                    this@SinchService,
                    IncomingCallScreenActivity::class.java
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(
                    IncomingCallScreenActivity.EXTRA_ID,
                    IncomingCallScreenActivity.MESSAGE_ID
            )
            intent.putExtra(CALL_ID, call.callId)
            val inForeground = isAppOnForeground(applicationContext)
            if (!inForeground) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !inForeground) {
                (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
                        IncomingCallScreenActivity.MESSAGE_ID,
                        createIncomingCallNotification(call.remoteUserId, intent)
                )
            } else {
                this@SinchService.startActivity(intent)
            }
        }

        private fun isAppOnForeground(context: Context): Boolean {
            val activityManager =
                    context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val appProcesses =
                    activityManager.runningAppProcesses ?: return false
            val packageName = context.packageName
            for (appProcess in appProcesses) {
                if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == packageName) {
                    return true
                }
            }
            return false
        }

        private fun getBitmap(context: Context, resId: Int): Bitmap {
            val largeIconWidth = context.resources
                    .getDimension(R.dimen.notification_large_icon_width).toInt()
            val largeIconHeight = context.resources
                    .getDimension(R.dimen.notification_large_icon_height).toInt()
            val d = context.resources.getDrawable(resId)
            val b =
                    Bitmap.createBitmap(largeIconWidth, largeIconHeight, Bitmap.Config.ARGB_8888)
            val c = Canvas(b)
            d.setBounds(0, 0, largeIconWidth, largeIconHeight)
            d.draw(c)
            return b
        }

        private fun getPendingIntent(intent: Intent, action: String): PendingIntent {
            intent.action = action
            return PendingIntent.getActivity(
                    applicationContext,
                    111,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
        }

        private fun createIncomingCallNotification(
                userId: String,
                fullScreenIntent: Intent
        ): Notification {
            val pendingIntent = PendingIntent.getActivity(
                    applicationContext,
                    112,
                    fullScreenIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            val builder =
                    NotificationCompat.Builder(
                            applicationContext,
                            FcmListenerService.CHANNEL_ID
                    )
                            .setContentTitle("Incoming call")
                            .setContentText(userId)
//                    .setLargeIcon(getBitmap(applicationContext, R.mipmap.ic_launcher))
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setPriority(NotificationCompat.PRIORITY_MAX)
                            .setContentIntent(pendingIntent)
                            .setFullScreenIntent(pendingIntent, true)
                            .addAction(
                                    R.drawable.button_accept,
                                    "Answer",
                                    getPendingIntent(
                                            fullScreenIntent,
                                            IncomingCallScreenActivity.ACTION_ANSWER
                                    )
                            )
                            .addAction(
                                    R.drawable.button_decline,
                                    "Ignore",
                                    getPendingIntent(
                                            fullScreenIntent,
                                            IncomingCallScreenActivity.ACTION_IGNORE
                                    )
                            )
                            .setOngoing(true)
            return builder.build()
        }
    }

    private inner class PersistedSettings(context: Context) {
        private val mStore: SharedPreferences
        var username: String?
            get() = mStore.getString("Username", "")
            set(username) {
                val editor = mStore.edit()
                editor.putString("Username", username)
                editor.commit()
            }

        init {
            mStore = context.getSharedPreferences(
                    Companion.PREF_KEY,
                    Context.MODE_PRIVATE
            )
        }
    }

    companion object {
        /*
IMPORTANT!

This sample application was designed to provide the simplest possible way
to evaluate Sinch Android SDK right out of the box, omitting crucial feature of handling
incoming calls via managed push notifications, which requires registering in FCM console and
procuring google-services.json in order to build and work.

Android 8.0 (API level 26) imposes limitation on background services and we strongly encourage
you to use Sinch Managed Push notifications to handle incoming calls when app is closed or in
background or phone is locked.

DO NOT USE THIS APPLICATION as a skeleton of your project!

Instead, use:
- sinch-rtc-sample-push (for audio calls) and
- sinch-rtc-sample-video-push (for video calls)
*/
        const val APP_KEY = "API_KEY"
        const val APP_SECRET = "APP_SECRET"
        const val ENVIRONMENT = "clientapi.sinch.com"
        const val MESSAGE_PERMISSIONS_NEEDED = 1
        const val REQUIRED_PERMISSION = "REQUIRED_PESMISSION"
        const val MESSENGER = "MESSENGER"
        const val CALL_ID = "CALL_ID"
        val TAG = SinchService::class.java.simpleName
        private const val PREF_KEY = "Sinch"
    }
}